package com.example.test_1

import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity



class MainActivity : AppCompatActivity() {


    private lateinit var name:EditText
    private lateinit var surname:EditText
    private lateinit var email:EditText
    private lateinit var pASSWORD:EditText
    private lateinit var checkbox1:CheckBox
    private lateinit var register:Button





    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        name = findViewById(R.id.editTextTextPersonName3)
        surname = findViewById(R.id.editTextTextPersonName4)
        email = findViewById(R.id.editTextTextEmailAddress2)
        pASSWORD = findViewById(R.id.editTextTextPassword2)
        checkbox1 = findViewById(R.id.checkBox2)
        register = findViewById(R.id.button2)


        register.setOnClickListener {

            val firstName = name.text.toString().trim()
            val lastname = surname.text.toString().trim()
            val eMail = email.text.toString().trim()
            val passWord = pASSWORD.text.toString().trim()
            val checkbox = checkbox1



            if(firstName.isEmpty()){
                name.error = "შეიყვანეთ სახელი"
                return@setOnClickListener
            }else if(lastname.isEmpty()){
                surname.error ="შეიყვანეთ გვარი"
                return@setOnClickListener
            }else if (eMail.isEmpty()){
                email.error ="შეიყვანეთ იმეილი"
                return@setOnClickListener
            }else if(passWord.isEmpty()){
                pASSWORD.error ="შეიყვანეთ პაროლი"
                return@setOnClickListener
            }else if(passWord.length <= 6){
                pASSWORD.error ="პაროლი უნდა შედგებოდეს მინიმუმ 6 სიმბოლოსგან"
                return@setOnClickListener
            }else if(!checkbox.isChecked){
                checkbox.error ="გთხოვთ დაეთანხმოთ პირობებს"
                return@setOnClickListener

            }else {
                Toast.makeText(this, "მუშაობს", Toast.LENGTH_SHORT).show()

            }



        }


    }

}